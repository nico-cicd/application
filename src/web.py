
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 14 16:52:54 2023
app web
@author: nico
"""

from flask import Flask, flash, redirect, render_template, request, session, abort
from ip_adresse import recuperer_adresse_ip

#configure the app
app = Flask(__name__)

#main page
@app.route("/")
def index():
    words = ["hello", "visitor", "thank", "you", "I", "love", "bunny", "I", "love", "Goose", "But", "I", "love", "more", "bunnies"]
    return render_template('index.html', title="welcome", words=words)

#run the app
app.run(host="0.0.0.0", port=5000)