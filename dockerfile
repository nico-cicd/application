FROM python:3.12-rc-slim-bullseye

# Installation library python
RUN pip install flask

# Declaration de l'espace de travail
WORKDIR /code

# Copie de l'application en local vers le container
COPY ./src /code/src

# Commande executer lors du lancement du container
ENTRYPOINT ["python3", "src/web.py"]

# Pour se connecter a l'appli : http://127.0.0.1:5000/