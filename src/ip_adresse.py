import socket

def recuperer_adresse_ip():
    hostname = socket.gethostname()
    ip_addresse = socket.gethostbyname(hostname)
    return  ip_addresse
